# TOH

My implementation of project from official Angular documentation. Tutorial can be found
[here](https://angular.io/tutorial).

## Getting Started

### Prerequisites

To successfully use development version of this project you will need to install NodeJS and Angular CLI.

Please refer to [this](https://nodejs.org/en/) site for instructions on how to install NodeJS on your system.

After installing NodeJS install Angular CLI with:

```
npm install -g @angular/cli
```

### Installing

Clone repository and move to project root.

Install dependencies.

```
npm install
```

Run development version of a project.

```
ng serve
```

## Running the tests

Run command below to execute tests.

```
ng test
```

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of
conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags).

## Authors

Initial content for this project was provided by Matic Zagmajster. For more information please see [AUTHORS]() file.

## License

Sse [LICENSE.md](LICENSE.md) file for details about license used in this project.

## Acknowledgments

* Big tanks to Angular team and [@johnpapa](https://github.com/johnpapa) for an awesome introduction to Angular.
