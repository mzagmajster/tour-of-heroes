import { AtPage } from './app.po';

describe('at App', () => {
  let page: AtPage;

  beforeEach(() => {
    page = new AtPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
