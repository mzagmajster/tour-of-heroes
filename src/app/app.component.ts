import { Component, OnInit } from '@angular/core';

import { Hero } from './hero';
import { HeroService } from './hero.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  title: string;

  constructor(private service: HeroService) {
    this.title        = 'Tour of heroes';
  }

  ngOnInit() {
  }
}
